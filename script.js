
/** JAVASCRIPT DU JEU DE MEMOIRE*/

const nom = document.getElementById('nom').value
const nbPaires = document.getElementById('nb-paires').value
const erreurElement = document.getElementById('erreur')
const soumettre = document.getElementById('soumettre')
soumettre.addEventListener('click', validerFormulaire)
const regex = /^\w+$/

// Section Validation du formulaire (messages erreurs)//

/**
 *La fonction  va permettre de valider le formulaire et
 *envoyer les données pour créer le Jeu.
 *
 * @param {string} e va permettre d'arreter l'envoie des données en cas d'erreur
 */
function validerFormulaire (e) {
  const messages = []
  const nomValide = regex.test(nom)
  if (nomValide === false) {
    messages.push('Nom doit etre alphanumerique')
  }


  if (nbPaires < 2 || nbPaires > 10) {
    messages.push('Le nombre de paires doit se situer entre 2 et 10')
  }

  if (messages.length > 0) {
    e.preventDefault()
    erreurElement.innerText = messages.join(', ')
  } else {
    document.getElementById('formulaire').style.display = 'none'
    CreerTableau()
    demarrerTimer()
  }
}

// options de cartes

/** 
 * Je n'ai malheureusement pas trouvé comment faire le lien avec nbPaires.
 * J'ai une idée générale d'utiliser un tableau vide et de faire un .push
 * mais avec le contexte des images et le tableau de carteArray,
 * je n'arrive pas a trouver comment utiliser les images pour faire la meme chose.
 *
*/
const carteArray = [
  {
    name: 'numero 1',
    img: 'images/numero_1.png'
  },
  {
    name: 'numero 1',
    img: 'images/numero_1.png'
  },
  {
    name: 'numero 2',
    img: 'images/numero_2.png'
  },
  {
    name: 'numero 2',
    img: 'images/numero_2.png'
  },
  {
    name: 'numero 3',
    img: 'images/numero_3.png'
  },
  {
    name: 'numero 3',
    img: 'images/numero_3.png'
  },
  {
    name: 'numero 4',
    img: 'images/numero_4.png'
  },
  {
    name: 'numero 4',
    img: 'images/numero_4.png'
  },
  {
    name: 'numero 5',
    img: 'images/numero_5.png'
  },
  {
    name: 'numero 5',
    img: 'images/numero_5.png'
  },
  {
    name: 'numero 6',
    img: 'images/numero_6.png'
  },
  {
    name: 'numero 6',
    img: 'images/numero_6.png'
  },
  {
    name: 'numero 7',
    img: 'images/numero_7.png'
  },
  {
    name: 'numero 7',
    img: 'images/numero_7.png'
  },
  {
    name: 'numero 8',
    img: 'images/numero_8.png'
  },
  {
    name: 'numero 8',
    img: 'images/numero_8.png'
  },
  {
    name: 'numero 9',
    img: 'images/numero_9.png'
  },
  {
    name: 'numero 9',
    img: 'images/numero_9.png'
  },
  {
    name: 'numero 10',
    img: 'images/numero_10.png'
  },
  {
    name: 'numero 10',
    img: 'images/numero_10.png'
  }
]

/* RANDOMIZER */
carteArray.sort(() => 0.5 - Math.random())

/* QUERY DE LA GRID ET DU SCORE, TABLEAU DES CARTES CHOISI */
const grid = document.querySelector('.grid')
const resultDisplay = document.querySelector('#result')
let cartechoisi = []
let cartechoisiId = []
const cartegagnee = []

// Creation du tableau//

/**
 * Cette fonction permet de creer le tableau dans lequel
 * les cartes se retrouvent 
 * 
 */
function CreerTableau () {
  for (let i = 0; i < carteArray.length; i++) {
    const carte = document.createElement('img')
    carte.setAttribute('src', 'images/blank.png')
    carte.setAttribute('data-id', i)
    carte.addEventListener('click', flipcarte)
    grid.appendChild(carte)
  }
}

// voir si les cartes matchent //

/**
 *cette fonction permet de voir si les cartes matchent ou pas
 *si les cartes sot pareil, la carte tourne bleue, sinbn,
 *elle retourne a l'etat de depart.
 *Si il n'y a pas de paires a matcher, la partie est terminée
 */
function checkForMatch () {
  const carte = document.querySelectorAll('img')
  const optionUnId = cartechoisiId[0]
  const optionDeuxId = cartechoisiId[1]
  if (cartechoisi[0] === cartechoisi[1]) {
    carte[optionUnId].setAttribute('src', 'images/blue.png')
    carte[optionDeuxId].setAttribute('src', 'images/blue.png')
    cartegagnee.push(cartechoisi)
  } else {
    carte[optionUnId].setAttribute('src', 'images/blank.png')
    carte[optionDeuxId].setAttribute('src', 'images/blank.png')
  }
  cartechoisi = []
  cartechoisiId = []
  resultDisplay.textContent = cartegagnee.length
  if (cartegagnee.length === carteArray.length / 2) {
    resultDisplay.textContent = 'Bravo, vous avez gagnee!'
  }
}

// Flipper les cartes//
/**
 *Flip de la carte. Permet de definir lorsque l'on choisit 2 cartes,
 *les 2 cartes sont vérifiées dans la fonction check for match
 *avant de decider de leur sort. Temps de "decision" de 1 minute.
 *
 *
 */
function flipcarte () {
  const carteId = this.getAttribute('data-id')
  cartechoisi.push(carteArray[carteId].name)
  cartechoisiId.push(carteId)
  this.setAttribute('src', carteArray[carteId].img)
  if (cartechoisi.length === 2) {
    setTimeout(checkForMatch, 1000)
  }
}

/* Section Timer 1 minute */

/**
 * Demarrer le Timer lors du demarrage du jeu.
 * le jeu demarre lorsque le formulaire est validée (else) en meme temps
 * que la creation du jeu.
 *
 */
function demarrerTimer () {
  const nbMinutes = 1
  const temps = nbMinutes * 60
  MiseAJourAffichage(temps)
  const intervale = setInterval(diminuerde1Seconde, 1000)
  return intervale


function diminuerde1Seconde () {
  let nbSecondes = document.getElementById('timer').getAttribute('data-secondes')
  nbSecondes = parseInt(nbSecondes)
  nbSecondes = nbSecondes - 1
  MiseAJourAffichage(nbSecondes)
  if (nbSecondes === 0) {
    clearInterval(intervale)
    alert('Temps ecoulee, vous avez perdu!')
    location.reload()
  }
}

function MiseAJourAffichage (nbSecondes) {
  let secondes = nbSecondes % 60
  const minutes = Math.floor(nbSecondes / 60)
  if (secondes < 10) {
    secondes = '0' + secondes
  }
  const timer = document.getElementById('timer')
  timer.textContent = minutes + ':' + secondes
  timer.setAttribute('data-secondes', nbSecondes)
}
}

